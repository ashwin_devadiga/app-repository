package com.example.weatherapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weatherapp.Adapters.RecyclerViewAdapter
import com.example.weatherapp.R
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.utils.Coroutines
import kotlinx.android.synthetic.main.fragment_favourite.*
import kotlinx.android.synthetic.main.fragment_favourite.view.*


class FavouriteFragment(var history: List<Weather>?) : Fragment(){


    var buttonText: String? = null
    var message: String?= null
    lateinit var adapter: RecyclerViewAdapter
    var i =10
    var temp = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_favourite, container, false)


        message = this.arguments?.getString("Tag")
        buttonText = this.arguments?.getString("Message")

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        removeButton.setText(message)

        val recyclerView = view.recyclerView
        if(message == "Clear All"){
            tvMessage.setText("$buttonText")
        }
        else {
            tvMessage.setText("${history?.size} $buttonText")
        }

        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = RecyclerViewAdapter(history, this)
        recyclerView.adapter = adapter
        val alertDialogBuilder = AlertDialog.Builder(requireContext())

        //alert dialog
        alertDialogBuilder.setMessage(R.string.alert)
        alertDialogBuilder.setPositiveButton("Yes"){
                dialog, which ->
            Coroutines.io {
                if(temp == 0){
                val db = AppDatabase(requireContext())
                db.getUserDao().updateFavourite()}
                else {
                    val db = AppDatabase(requireContext())
                    db.getUserDao().updateRecent()
                }
            }
            history = null
            adapter.updateView(message)

            adapter.notifyItemRangeRemoved(0,10)


        }
        alertDialogBuilder.setNegativeButton("No"){
                dialog, which ->
            return@setNegativeButton

        }

        removeButton.setOnClickListener {

            if(removeButton.text == "Clear All"){
                temp = 1
            }
            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()

        }

    }
}
