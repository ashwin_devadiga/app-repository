package com.example.weatherapp.Adapters

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.weatherapp.R
import com.example.weatherapp.fragment.FavouriteFragment
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.view.FavouriteActivity
import com.example.weatherapp.view.HomeScreenActivity
import com.example.weatherapp.view.RecentSearchActivity
import kotlinx.android.synthetic.main.cardview_layout.view.*


class RecyclerViewAdapter(
    var favourite: List<Weather>?,
    val context: FavouriteFragment
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.cardview_layout, parent, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return favourite?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvDescription.setText(favourite!![position].description)
        holder.itemView.tvTemperature.setText("${favourite!![position].temperature}")
        holder.itemView.tvLocation.setText(favourite!!.get(position).location)
        val imageView = holder.itemView.ivImage
        setDescription(imageView, favourite!![position].weather_id)
        if (favourite!!.get(position).favourite == 0)
        {
            holder.itemView.ivFavourite.setImageResource(R.drawable.icon_favourite)
        }


        holder.itemView.cardView.setOnClickListener {
            val _history = favourite!![position]
            Log.d("favourite", favourite!![position].description)
            val intent = Intent(context.requireContext(),HomeScreenActivity::class.java)
            intent.putExtra("CityId", favourite!![position].city_id)
            intent.putExtra("Weather", favourite!![position].weather_id)
            intent.putExtra("Location", favourite!![position].location)
            intent.putExtra("Description", favourite!![position].description)
            intent.putExtra("Tempeture", favourite!![position].temperature)
            intent.putExtra("Temp_min", favourite!![position].temp_min)
            intent.putExtra("Temp_max", favourite!![position].temp_max)
            intent.putExtra("Humidity", favourite!![position].humidity)
            intent.putExtra("Precipitation", favourite!![position].precipitation)
            intent.putExtra("Favourite", favourite!![position].favourite)
            intent.putExtra("Id", favourite!![position].id)
            context.startActivity(intent)
        }
        holder.itemView.ivFavourite.setOnClickListener {
          // AppDatabase.invoke(context.requireContext()) .getUserDao().s
        }

    }
    fun updateView(message: String?) {
        if(message.equals("Clear All")) {
            context.startActivity(
                Intent(
                    context.requireContext(),
                    RecentSearchActivity::class.java
                )
            )
        }else
        {
            context.startActivity(
                Intent(
                    context.requireContext(),
                    FavouriteActivity::class.java
                )
            )

        }

    }


    fun setDescription(ivWeather: ImageView, id: Int) {
        if(id < 300){
            ivWeather.setBackgroundResource(R.drawable.icon_thunderstorm_small)
        }else if (id < 500){
            ivWeather.setBackgroundResource(R.drawable.icon_rain_small)
        }else if(id < 600){
            ivWeather.setBackgroundResource(R.drawable.icon_rain_small)
        }else if(id < 700){
            ivWeather.setBackgroundResource(R.drawable.icon_partly_cloudy_small)
        }else if(id < 800){
            ivWeather.setBackgroundResource(R.drawable.icon_partly_cloudy_small)
        }else if(id == 800){
            ivWeather.setBackgroundResource(R.drawable.icon_mostly_sunny_small)
        }else
        {
            ivWeather.setBackgroundResource(R.drawable.icon_mostly_cloudy_small)

        }

    }




}