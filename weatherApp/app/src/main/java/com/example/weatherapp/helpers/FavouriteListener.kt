package com.example.weatherapp.helpers

import com.example.weatherapp.model.datamodel.Response

interface FavouriteListener {
    val favourite: ArrayList<Response>
}