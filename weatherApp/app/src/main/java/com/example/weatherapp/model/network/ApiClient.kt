package com.example.weatherapp.model.network

import android.content.Context
import com.example.weatherapp.model.datamodel.WeatherResponse
import com.example.weatherapp.utils.hasNetwork
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface WeatherApi {

  @GET("weather?")
  suspend fun currentWeather(
      @Query("q") q: String,
      @Query("appid") appid: String
  ): Response<WeatherResponse>



    companion object {
        operator fun invoke(
            context: Context
        ): WeatherApi{
            val cacheSize = (5 * 1024 * 1024).toLong()
            val myCache = Cache(context.cacheDir, cacheSize)

            val okHttpClient = OkHttpClient.Builder()
                .cache(myCache)
                .addInterceptor { chain ->
                    var request = chain.request()
                    request = if (hasNetwork(context)!!)
                        request.newBuilder().header("Cache-Control", "public, max-age=" + 60 * 30).build()
                    else
                        request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 30).build()
                    chain.proceed(request)
                }
                .build()


          /*  val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(networkConnectionInterceptor)
                .build()*/

            return  Retrofit.Builder().client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.openweathermap.org/data/2.5/").build()
                .create(WeatherApi::class.java)

        }
    }
}
