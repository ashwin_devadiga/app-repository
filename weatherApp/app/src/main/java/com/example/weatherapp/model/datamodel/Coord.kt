package com.example.weatherapp.model.datamodel

data class Coord(
    val lat: Double,
    val lon: Double
)