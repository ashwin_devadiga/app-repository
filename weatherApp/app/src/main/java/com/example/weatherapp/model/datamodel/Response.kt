package com.example.weatherapp.model.datamodel

data class Response(
    var id: String? = null,
    var main: String? = null,

    var temp: String? = null,

    var temp_min: String? = null,

    var temp_max: String? = null,

    var humidity: String? = null

)