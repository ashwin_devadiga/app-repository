package com.example.weatherapp.helpers

interface HomeScreenListener {
    fun onStarted()
    fun onSuccess(s: String)
    fun onFailure(s: String)
    fun setDescription(id: Int)

}