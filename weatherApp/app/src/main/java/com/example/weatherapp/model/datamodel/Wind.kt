package com.example.weatherapp.model.datamodel

data class Wind(
    val deg: Int,
    val speed: Double
)