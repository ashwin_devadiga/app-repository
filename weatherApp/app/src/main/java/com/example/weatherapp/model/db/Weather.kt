package com.example.weatherapp.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "user_data")
data class Weather (
    @PrimaryKey(autoGenerate = false)

    var city_id: Int,

    var weather_id: Int,

    var location: String,

    var description: String,

    var temperature: Double,

    var temp_min: Double,

    var temp_max: Double,

    var humidity: Int,

    var precipitation: Int,

    var favourite: Int,

    var id:Int

)

