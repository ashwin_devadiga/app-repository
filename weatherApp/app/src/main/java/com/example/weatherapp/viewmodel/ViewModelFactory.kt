package com.example.weatherapp.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.repositories.UserRepository

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val repository: UserRepository,
    private val db: AppDatabase

): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeScreenViewModel(repository, db) as T
    }
}