package com.example.weatherapp.view

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.weatherapp.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        Handler().postDelayed(
            {
            startActivity(Intent(this, HomeScreenActivity::class.java ), ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                finish()
        }, 3000)
    }
}
