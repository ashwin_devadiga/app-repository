package com.example.weatherapp.model.repositories


import androidx.lifecycle.LiveData
import com.example.weatherapp.model.datamodel.WeatherResponse
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.model.network.SafeApiRequest
import com.example.weatherapp.model.network.WeatherApi
import com.example.weatherapp.model.preference.PreferenceProvider
import com.example.weatherapp.utils.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class UserRepository (
    private val api: WeatherApi,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
): SafeApiRequest() {


    suspend fun currentWeather(location: String, appid: String): WeatherResponse? {
        var response: WeatherResponse? = null
        response = apiRequest { api.currentWeather(location, appid) }
        return response
    }

    suspend fun getWeather(cityId: Int?): LiveData<Weather>{
        return withContext(Dispatchers.IO) {
            db.getUserDao().get(cityId)

        }
    }


    suspend fun saveWeather(
        weather: List<Weather>
    ) {
        Coroutines.io {
            db.getUserDao().insert(weather)
        }
    }



}
