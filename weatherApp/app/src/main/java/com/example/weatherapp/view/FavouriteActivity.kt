package com.example.weatherapp.view



import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import com.example.weatherapp.R
import com.example.weatherapp.fragment.BlankFragment
import com.example.weatherapp.fragment.FavouriteFragment
import com.example.weatherapp.helpers.FavouriteListener
import com.example.weatherapp.model.datamodel.Response
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.utils.Coroutines
import kotlinx.android.synthetic.main.activity_homescreen.*
import java.util.ArrayList


class FavouriteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        var temp = 1
        var size = 0


        supportActionBar?.title = "Favourite"
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#FFFFFF")))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black)


        val bundle: Bundle? = intent.extras

        var db = AppDatabase(this)
        var favourites: List<Weather>? = null

        Coroutines.io {
            favourites = db.getUserDao().getFavourites() as List<Weather>

            size = favourites!!.size

            if (size == 0) {
                loadFragment(BlankFragment(), "No Favourites added")
                temp = 1
            } else {
                loadFragment(FavouriteFragment(favourites), "Remove All")
                temp = 0
            }

        }
    }

    fun loadFragment(fragment: Fragment,tag: String) {
        val bundle: Bundle = Bundle()
        bundle.putString("Tag", tag)
        bundle.putString("Message","City added as favourite")
        fragment.arguments = bundle

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameLayout, fragment, tag)
        fragmentTransaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.app_bar_search -> {
                Log.d("Activity", "hello")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {

        val intent = Intent(this,
            HomeScreenActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
        return true
    }
}
