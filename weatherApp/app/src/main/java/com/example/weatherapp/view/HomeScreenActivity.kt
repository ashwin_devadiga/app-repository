package com.example.weatherapp.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.weatherapp.R
import com.example.weatherapp.databinding.ActivityHomescreenBinding
import com.example.weatherapp.helpers.HomeScreenListener
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.model.network.NetworkConnectionInterceptor
import com.example.weatherapp.model.network.WeatherApi
import com.example.weatherapp.model.preference.PreferenceProvider
import com.example.weatherapp.model.repositories.UserRepository
import com.example.weatherapp.utils.Coroutines
import com.example.weatherapp.viewmodel.HomeScreenViewModel
import com.example.weatherapp.viewmodel.ViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import kotlinx.android.synthetic.main.activity_homescreen.*
import java.text.SimpleDateFormat
import java.util.*

class HomeScreenActivity() : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, HomeScreenListener {

    lateinit var viewModel: HomeScreenViewModel
    var currentLocation: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      //  setContentView( R.layout.activity_homescreen)
        var temp = 1

        val binding: ActivityHomescreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_homescreen)
       // val networkConnectionInterceptor = NetworkConnectionInterceptor(this)
        val api = WeatherApi(this)
        val db = AppDatabase(this)
        val prefs= PreferenceProvider(this)
        val repository = UserRepository(api, db, prefs)
        val factory = ViewModelFactory(repository, db)
        supportActionBar?.hide()

        navigation_view.setNavigationItemSelectedListener(this)

        viewModel = ViewModelProviders.of(this, factory).get(HomeScreenViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
        viewModel.homeScreenListener = this
        val bundle = intent.extras
        var _weather: Weather? = null


        bundle?.let {
                bundle->

            val city_id: Int = bundle!!.getInt("CityId")
            val weather_id: Int = bundle!!.getInt("Weather")
            val location: String = bundle.getString("Location").toString()
            val description: String? = bundle.getString("Description")
            val temperature: Double? =  bundle.getDouble("Tempeture")
            val temp_min: Double? = bundle.getDouble("Temp_min")
            val temp_max: Double = bundle.getDouble("Temp_max")
            val humidity: Int = bundle.getInt("Humidity")
            val precipitation: Int = bundle.getInt("Precipitation")
            val favourite: Int = bundle.getInt("Favourite")
            val id:Int = bundle.getInt("Id")
            temp = favourite
            Log.d("intent", "${precipitation},${description}")
           // viewModel.getWeather(favouriteButton)
           // Log.d("bunnn", "hello")
             _weather = Weather(city_id,weather_id,location,description!!,temperature!!,temp_min!!,temp_max,humidity,precipitation,favourite,id)
            Log.d("bunnn", _weather.toString())
           tvLocation.text = "ggg"

        }

        tvLocation.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(bundle == null) {
                    val _currentLocation = tvLocation.text.toString()
                    var id = 0
                    if (currentLocation.equals(_currentLocation)) {
                        id = 1
                        viewModel.currentWeather(tvLocation.text.toString(), id)
                    }else
                    {
                        id =2
                        viewModel.currentWeather(tvLocation.text.toString(), id)

                    }

                }
                else
                {
                    //Log.d("bunnn", "hello")
                   viewModel.historyDetails(_weather!!, favouriteButton)
                     }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })





// permission
        if(bundle == null) {

            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                getCurrentLocation(tvLocation)
            } else {
                ActivityCompat.requestPermissions(
                    this as Activity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    100
                )

            }
            val date = getDate().toString()
            tvDate.setText(date)
        }
//favourite

        favouriteButton.setOnClickListener {
            if(temp == 1) {
                it.setBackgroundResource(R.drawable.icon_favourite_active)
                Coroutines.io {
                    viewModel.insertFavourite(tvLocation.text.toString())
                }

                temp = 0
            } else
            {
                it.setBackgroundResource(R.drawable.icon_favourite)
                val favourite = 0
                viewModel.delete(tvLocation.text.toString(),favourite)
                temp = 1
            }
        }


        sideMenu.setOnClickListener {
            navigation_view.visibility =View.VISIBLE
        }

        farenheitButton.setOnClickListener {
            viewModel.temperatureConverter("farenheit")
            farenheitButton.let {
                it.setBackgroundResource(R.color.White)
                it.setTextColor(Color.parseColor("#E32843"))

            }
            celciusButton.let {
                it.setBackgroundResource(R.color.Transparent)
                it.setTextColor(Color.parseColor("#FFFFFF"))
            }
        }

        celciusButton.setOnClickListener {
            viewModel.temperatureConverter("celcius")
            farenheitButton.let {
                it.setBackgroundResource(R.color.Transparent)
                it.setTextColor(Color.parseColor("#FFFFFF"))
            }
            celciusButton.let {
                it.setBackgroundResource(R.color.White)
                it.setTextColor(Color.parseColor("#E32843"))
            }
        }

        //search
        searchBar.setOnClickListener {
            val placeOption = PlaceOptions.builder().country("IN")
                .backgroundColor(Color.parseColor("#EEEEEE"))
                .limit(10)
                .build(PlaceOptions.MODE_CARDS)
            val intent = PlaceAutocomplete.IntentBuilder()
                .accessToken(getString(R.string.MAPBOX_ACCESS_TOKEN))
                .placeOptions(placeOption)
                .build(this)
            startActivityForResult(intent, 200)
        }
    }

   override fun setDescription(id: Int) {


            if(id < 300){
                ivWeather.setBackgroundResource(R.drawable.icon_thunderstorm_big)


            }else if (id < 500){
                ivWeather.setBackgroundResource(R.drawable.icon_rain_big)


            }else if(id < 600){
                ivWeather.setBackgroundResource(R.drawable.icon_rain_big)


            }else if(id < 700){
                ivWeather.setBackgroundResource(R.drawable.icon_partially_cloudy_big)


            }else if(id < 800){
                ivWeather.setBackgroundResource(R.drawable.icon_mostly_sunny)


            }else if(id == 800){
                ivWeather.setBackgroundResource(R.drawable.icon_mostly_sunny)


            }else
            {
                ivWeather.setBackgroundResource(R.drawable.icon_mostly_cloudy_big)

            }


    }

    private fun getDate(): Any {
        val date: Date = Date()
        val simpleDateFormat = SimpleDateFormat("E, dd MMM yyyy  hh:mm a ", Locale.US)
        val currentDateTime = simpleDateFormat.format(date)
        return currentDateTime
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return true
    }


    override fun onNavigationItemSelected(item : MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_home ->{
               navigation_view.visibility = View.INVISIBLE

            }
            R.id.nav_favourite -> {
             //   Log.d("Activity", "hello")
                val intent =Intent(this, FavouriteActivity::class.java)
               // Log.d("favourie", viewModel.favouriteList.toString())
                intent.putStringArrayListExtra("Favourite", viewModel.favouriteList as ArrayList<String>)
                startActivity(intent)
            }
            R.id.nav_recentsearch -> {
                val intent =Intent(this, RecentSearchActivity::class.java)
                intent.putExtra("Favourite", viewModel.favouriteList)
                startActivity(intent)
            }
        }
        return true

    }

    fun getCurrentLocation(tvLocation: TextView){
        val fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val location: Location = it.getResult()
                val geocoder: Geocoder = Geocoder(this)
                val address: List<Address> = geocoder.getFromLocation(location.latitude, location.longitude, 1)
                currentLocation = "${address.get(0).subAdminArea}, ${address.get(0).adminArea}"
                tvLocation.text = currentLocation
            }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.size>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation(tvLocation)
            }
        }else{
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
        return;
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
       if (resultCode == Activity.RESULT_OK) {
            val feature = PlaceAutocomplete.getPlace(data)
            if(requestCode == 200){
                tvLocation.text = feature.text()
                favouriteButton.setBackgroundResource(R.drawable.icon_favourite)
               // val id = 2
               // viewModel.currentWeather(feature.placeName().toString(),id)
            }
        }


    }

    override fun onStarted() {
        Toast.makeText(this,"Started", Toast.LENGTH_SHORT).show()

    }

    override fun onSuccess(s: String) {

            Toast.makeText(this,s , Toast.LENGTH_SHORT).show()
            Log.d("Activity", s.toString())


    }

    override fun onFailure(s: String) {
        Toast.makeText(this,"$s", Toast.LENGTH_SHORT).show()
    }

}
