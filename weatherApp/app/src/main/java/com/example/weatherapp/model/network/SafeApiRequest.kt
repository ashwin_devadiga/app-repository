package com.example.weatherapp.model.network

import com.example.weatherapp.utils.ApiException
import retrofit2.Response

abstract class SafeApiRequest
{
    var message: String? = null
    suspend fun<T: Any> apiRequest(call: suspend() -> Response<T>): T{
        val response = call.invoke()
        if(response.isSuccessful){
            return response.body()!!
        }
        else
        {
            //handle exception
            message = "Error Code: ${response.code()}"
            throw ApiException(message.toString())
        }
    }
}



