package com.example.weatherapp.model.datamodel

data class CurrentWeather(
    var id: Int? = null,
    var main: String? = null,
    var temp: Double? = null,
    var temp_min: Double? = null,
    var temp_max: Double? = null,
    var humidity: Int? = null
)