package com.example.weatherapp.model.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weather: List<Weather>)

   @Query("SELECT * FROM user_data ORDER BY id ASC")
    fun getWeather(): List<Weather>

    @Delete
    fun Delete(weather: Weather)

    @Query("DELETE FROM user_data WHERE id = 2 ")
    fun deleteRececnt()

    @Query("DELETE FROM user_data WHERE favourite = 1 ")
    fun deletefavourite()

    @Query("SELECT * FROM user_data WHERE id = 2 ")
    fun getReccent(): List<Weather>

    @Query("SELECT * FROM user_data WHERE  favourite = 1 ")
    fun getFavourites(): List<Weather>

    @Query("SELECT * FROM user_data WHERE city_id =:cityId")
    fun get(cityId: Int?): LiveData<Weather>



    @Query("UPDATE user_data SET favourite = 0")
    fun updateFavourite()

 @Query("UPDATE user_data SET id = 0")
 fun updateRecent()


}