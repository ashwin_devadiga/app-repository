package com.example.weatherapp.viewmodel


import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.*
import com.example.weatherapp.R
import com.example.weatherapp.helpers.HomeScreenListener
import com.example.weatherapp.model.db.AppDatabase
import com.example.weatherapp.model.db.Weather
import com.example.weatherapp.model.repositories.UserRepository
import com.example.weatherapp.utils.ApiException
import com.example.weatherapp.utils.Coroutines
import com.example.weatherapp.utils.NoInternetException
import kotlinx.coroutines.launch


class HomeScreenViewModel(
    private val repository: UserRepository,
    private val db: AppDatabase
): ViewModel() {
    var location = MutableLiveData<String>()
    val temperature = MutableLiveData<String>()
    var minmaxTemperature = MutableLiveData<String>()
    var precipitation = MutableLiveData<String>()
    val humidity = MutableLiveData<String>()
    val description = MutableLiveData<String>()

    val appid = "bf728431476b04c85644c4482e4e1b61"
    var favouriteList = ArrayList<String>()
    var city_Id: Int? = null
    var weatherId: Int? = null
    var temp_min: String? = null
    var temp_max: String? = null
    var main: String? = null
    var _humidity: Int? = null
    var _precipitaton: Int? = null
    var favourite = 0


    var temp: Double? = null
    var homeScreenListener: HomeScreenListener? = null


    fun currentWeather(currentLocation: String, id: Int) {
        homeScreenListener?.onStarted()

        Coroutines.main {
            try {
                val weatherResponse = repository.currentWeather(currentLocation, appid)

                weatherResponse!!.main.let {
                    temp = it.temp - 273.15
                    temperature.value = temp!!.toInt().toString()
                    temp_min = it.temp_min.minus(273.15).toString()
                    temp_max = it.temp_max.minus(273.15).toString()
                    location.value = currentLocation
                    minmaxTemperature.value =
                        "${it.temp_min.minus(273.15).toInt()}\u02DA-${it.temp_max.minus(273.15).toInt()}\u02DA"
                    humidity.value = "${it.humidity}%"
                    _humidity = it.humidity
                    homeScreenListener?.onSuccess("success")
                }
                city_Id = weatherResponse.id
                _precipitaton = weatherResponse.clouds.all
                precipitation.value = "${weatherResponse.clouds.all.toString()}%"
                weatherId = weatherResponse.weather.get(0).id
                description.value = weatherResponse.weather.get(0).main
                main = weatherResponse.weather.get(0).main

                homeScreenListener?.setDescription(weatherId!!)

                repository.saveWeather(
                    listOf(
                        Weather(city_Id!!, weatherId!!, location.value!!, description.value.toString(), temperature.value!!.toDouble(), temp_min!!.toDouble(), temp_max!!.toDouble(), _humidity!!, _precipitaton!!, favourite, id
                        )
                    )
                )

            } catch (e: ApiException) {
                homeScreenListener?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                homeScreenListener?.onFailure(e.message!!)
            }

        }

    }

    fun temperatureConverter(s: String) {
        if (s.equals("farenheit")) {
            temperature.value = ((temp?.times(1.8))?.plus(32))?.toInt().toString()
            return
        }
        temperature.value = temp?.toInt().toString()

    }

    suspend fun insertFavourite(location1: String) {
        repository.saveWeather(
            listOf(
                Weather(city_Id!!, weatherId!!, location.value!!, description.value.toString(), temperature.value!!.toDouble(), temp_min!!.toDouble(), temp_max!!.toDouble(), _humidity!!, _precipitaton!!,
                    1,
                    2
                )
            )
        )
    }


    fun delete(location: String, favourite: Int) {
        Coroutines.io {
            db.getUserDao().insert(
                listOf(
                    Weather(
                        city_Id!!,
                        weatherId!!,
                        location,
                        description.value.toString(),
                        temperature.value!!.toDouble(),
                        temp_min!!.toDouble(),
                        temp_max!!.toDouble(),
                        _humidity!!,
                        _precipitaton!!,
                        favourite,
                        2
                    )
                )
            )
        }
    }

    fun historyDetails(
        weather: Weather,
        favouriteButton: ImageView
    ) {
        temperature.value = weather.temperature.toInt().toString()
        minmaxTemperature.value = "${weather.temp_min!!.toInt()}˚-${weather.temp_max!!.toInt()}˚"
        precipitation.value = weather.precipitation.toString()
        humidity.value = "${weather.humidity}%"
        description.value = weather.description
        location.value = weather.location
        if (weather.favourite == 1) {
            favouriteButton.setImageResource(R.drawable.icon_favourite_active)
        } else
            favouriteButton.setImageResource(R.drawable.icon_favourite)
        homeScreenListener?.setDescription(weather.id)

    }
}

