package com.example.weatherapp.utils

import kotlinx.coroutines.*


object Coroutines {
    fun <T: Any> ioTheMain(work: suspend  (() -> T?), callback:  ((T?) -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            val data = CoroutineScope(Dispatchers.IO).async rt@{
                return@rt  work()
            }.await()
            callback(data)
        }

    fun main(work: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            work()
        }

    fun io(work: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.IO).launch {
            work()
        }

}